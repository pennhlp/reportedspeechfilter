#coding=utf-8
from nltk.corpus import stopwords
stop_words = stopwords.words('english')

import sys
reload(sys)
sys.setdefaultencoding('utf8')

import re, csv, string

pat1 = r'(\b[A-Z][a-z]*\s){4,}.*_url_'
pat2 = r'((-|\|)\s(([a-z0-9_]+\s?){1,4}))\Z'
pat3 = r'(^[\'"*~])|([\'"*~]$)'
#pat3 = r'(\A[\'"*~])|([\'"*~] (?!([a-z][\'"]))\Z)'
pat4 = r'^([a-z0-9_]+\s?){1,4}(~|:\s|\s-\s|([\-:][\'"]|\s[\'"])(?!([a-z\']+\s?){1,5}[\'"]\s))'
pat5 = r'(\b(via|by) (@|#))|@bloglovin'
pat6 = r'i (liked a @youtube video)|(added a video to a @youtube playlist)'
pat7 = r'^(\d\d?(?!\s?(years?|months?|weeks?|days?))|how|what|dear|inbox|new post|([a-z]+\s)?question|email|anonymous|fan|from a fan|from my inbox|check out.*video)\b.*_url_'
pat8 = r'((_url_\s?){2,})$'
pat9 = r'please hide my i d'

compiled_pat1 = re.compile(pat1)
compiled_pat2 = re.compile(pat2)
compiled_pat3 = re.compile(pat3)
compiled_pat4 = re.compile(pat4)
compiled_pat5 = re.compile(pat5)
compiled_pat6 = re.compile(pat6)
compiled_pat7 = re.compile(pat7)
compiled_pat8 = re.compile(pat8)
compiled_pat9 = re.compile(pat9)


def load_names(filepath):
    names = []
    infile = open(filepath)
    for line in infile:
        names.append(string.strip(line))
    return names

list_of_names = load_names('./names.txt')

def preprocess_pat1(tweet):
    tweet = re.sub('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', '_url_', tweet)
    tweet = re.sub(r'&amp;', " ", tweet)
    tweet = tweet.replace('“', '"').replace('”', '"').replace("‘", "'").replace("’", "'")
    for stw in stop_words:
        tweet = re.sub(r'\b' + stw + r'\b', "", tweet)
    tweet = re.sub("[^a-zA-Z_]", " ", tweet)
    for n in list_of_names:
        tweet = re.sub(r'\b' + n + r'\b', '_name_', tweet)
    tweet = re.sub(r'\s{2,}', " ", tweet)
    return tweet

def preprocess_pat2(tweet):
    tweet = re.sub('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', '_url_', tweet)
    tweet = re.sub(r'&amp;', " ", tweet)
    tweet = tweet.replace('“', '"').replace('”', '"').replace("‘", "'").replace("’", "'")
    tweet = tweet.lower()
    for stw in stop_words:
        tweet = re.sub(r'\b' + stw + r'\b', "", tweet)
    tweet = re.sub("[^-|a-z0-9_]", " ", tweet)
    tweet = re.sub(r'\s{2,}', " ", tweet)
    return tweet

def preprocess_pat3(tweet):
    tweet = re.sub('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', ' ', tweet)
    tweet = tweet.replace('“', '"').replace('”', '"').replace("‘", "'").replace("’", "'")
    tweet = tweet.lower()
    #tweet = re.sub(r'\A([\'"*~]([a-z]+\s?){1,6}[\'"*~])', " ", tweet)
    tweet = re.sub(r'([\'"*~]([a-z]+\s?){1,6}\W*[\'"*~])\Z', " ", tweet)
    tweet = tweet.strip()
    return tweet

def preprocess_pat4(tweet):
    tweet = re.sub('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', '_url_', tweet)
    #tweet = re.sub(r'&amp;', " ", tweet)
    tweet = tweet.replace('“', '"').replace('”', '"').replace("‘", "'").replace("’", "'")
    tweet = tweet.lower()
    #tweet = re.sub(regex_quote_stop, " ", tweet)
    #for stw in stop_words:
        #tweet = re.sub(r'\b' + stw + r'\b', " ", tweet)
    tweet = re.sub('[^-~a-z0-9_\'":)(\\][]', " ", tweet)
    tweet = re.sub(r'\s{2,}', " ", tweet)
    tweet = tweet.strip()
    return tweet

def preprocess_pat5(tweet):
    tweet = tweet.lower()
    tweet = re.sub(r'\s{2,}', " ", tweet)
    return tweet

def preprocess_pat6(tweet):
    tweet = tweet.lower()
    tweet = re.sub(r'\s{2,}', " ", tweet)
    return tweet

def preprocess_pat7(tweet):
    tweet = re.sub('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', '_url_', tweet)
    tweet = tweet.lower()
    tweet = re.sub("[^a-z0-9_@#]", " ", tweet)
    tweet = re.sub(r'\s{2,}', " ", tweet)
    tweet = tweet.strip()
    return tweet

def preprocess_pat8(tweet):
    tweet = re.sub('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', '_url_', tweet)
    tweet = re.sub(r'\s{2,}', " ", tweet)
    return tweet

def preprocess_pat9(tweet):
    tweet = tweet.lower()
    tweet = re.sub("[^a-z0-9_]", " ", tweet)
    tweet = re.sub(r'\s{2,}', " ", tweet)
    return tweet

def remove_reported_speech():
    with open ("coronavirus_matches_2020-04-06.tsv") as filename_input, open ("coronavirus_matches_filtered_2020-04-06.tsv", 'wb') as filename_output:
        filereader = csv.reader(filename_input, delimiter='\t')
        filewriter = csv.writer(filename_output, delimiter='\t')
        for row in filereader:
            text_pat1 = unicode(preprocess_pat1(row[2]))
            text_pat2 = unicode(preprocess_pat2(row[2]))
            text_pat3 = unicode(preprocess_pat3(row[2]))
            text_pat4 = unicode(preprocess_pat4(row[2]))
            text_pat5 = unicode(preprocess_pat5(row[2]))
            text_pat6 = unicode(preprocess_pat6(row[2]))
            text_pat7 = unicode(preprocess_pat7(row[2]))
            text_pat8 = unicode(preprocess_pat8(row[2]))
            text_pat9 = unicode(preprocess_pat9(row[2]))

            if not re.search(compiled_pat1, text_pat1):
                if not re.search(compiled_pat2, text_pat2):
                    if not re.search(compiled_pat3, text_pat3):
                        if not re.search(compiled_pat4, text_pat4):
                            if not re.search(compiled_pat5, text_pat5):
                                if not re.search(compiled_pat6, text_pat6):
                                    if not re.search(compiled_pat7, text_pat7):
                                        if not re.search(compiled_pat8, text_pat8):
                                            if not re.search(compiled_pat9, text_pat9):
                                                filewriter.writerow(row)
                                                #print row[2]




if __name__ == '__main__':
    remove_reported_speech()